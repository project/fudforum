FUDforum Integration module
==============================================================================

This module provides integration with FUDforum (http://fudforum.org/). 
The following (all optional) blocks are provided to integrate your forum with 
your drupal site:

1) FUDforum: New forum posts
   Display a list of the 10 latest forum messages.

2) FUDforum: Online forum users
   Display a list of all on-line forum users.

3) FUDforum: Forum statistics
   Display forum statistics including: number of users, threads, messages,
   newest member, etc.

Prerequisites 	 
------------------------------------------------------------------------------ 	 
This module assumes that FUDforum was configured to use PATH_INFO style URLs.
See setting in FUDForum's Admin Control Panel. If not, you will have to change 
the URL format in the module code yourself.

Installation
------------------------------------------------------------------------------
 
Follow these steps to install the FUDforum module:

  - Download the FUDforum module from http://drupal.org/project/fudforum

  - Copy fudforum.module to your modules directory (create a 
    subdirectory .../modules/fudforum/)

  - Navigate to Administer » modules and enable the fudforum module.

  - Navigate to Administer » settings » fudforum and enter the path to
    FUDforum's GLOBALS.php file. Ensure that Drupal will be able to read 
    this file.

  - Enable the blocks you want to use from Administer » blocks


Credits / Contacts
------------------------------------------------------------------------------

Module written by Frank Naude.

TODO List
------------------------------------------------------------------------------
 - Register new users created on Drupal to the forum DB

 - Support databases other than mysql

 - Translate this module into other languages.

