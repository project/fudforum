<?php
/**
 * @file
 * FUDforum integration module
 */

// error_reporting(E_NOTICE | E_ERROR | E_WARNING | E_PARSE | E_COMPILE_ERROR);
// ini_set('display_errors', '1');
// ini_set('display_startup_errors', 1);

function _fudforum_settings() {
  if (isset($GLOBALS['FUD_HOST'])) {
    return;  // Already loaded.
  }

  /* load forum config */
  $fudinc = variable_get('fudforum_inc', '/path/to/GLOBALS.php');
  if (!is_readable($fudinc)) {
    watchdog('FUDforum', 'Unable to read FUDforum\'s GLOBALS.php file. Please fix your settings!');
    return;
  }

  $data = file_get_contents($fudinc);
  eval(str_replace('<?php', '', substr_replace($data, '', strpos($data, 'require'))));

  /* Export variables required later */
  $GLOBALS['FUD_HOST']      = $GLOBALS['DBHOST'];
  $GLOBALS['FUD_USER']      = $GLOBALS['DBHOST_USER'];
  $GLOBALS['FUD_PASS']      = $GLOBALS['DBHOST_PASSWORD'];
  $GLOBALS['FUD_DB']        = $GLOBALS['DBHOST_DBNAME'];
  $GLOBALS['DBHOST_DBTYPE'] = $GLOBALS['DBHOST_DBTYPE'];
  $GLOBALS['FUD_PREFIX']    = $GLOBALS['DBHOST_TBL_PREFIX'];
  $GLOBALS['COOKIE_NAME']   = $GLOBALS['COOKIE_NAME'];
  $GLOBALS['COOKIE_DOMAIN'] = $GLOBALS['COOKIE_DOMAIN'];
  $GLOBALS['FUD_TIMEOUT']   = $GLOBALS['LOGEDIN_TIMEOUT'];
  $GLOBALS['FUD_URL']       = $GLOBALS['WWW_ROOT'] . variable_get('fudforum_root', 'index.php') . '/';
  $GLOBALS['FUD_URL']       = preg_replace('/\/\/$/', '/', $GLOBALS['FUD_URL']);
}

function _fudforum_connect() {
  _fudforum_settings();

  if (substr($GLOBALS['FUD_HOST'], 0, 1) == ':') {	// Socket connection.
    $socket = substr($GLOBALS['FUD_HOST'], 1);
    $GLOBALS['FUD_HOST'] = 'localhost';
  } else {
    $socket = NULL;
  }

  $fuddb = mysqli_connect($GLOBALS['FUD_HOST'], $GLOBALS['FUD_USER'], $GLOBALS['FUD_PASS'], $GLOBALS['FUD_DB'], NULL, $socket);
  if (mysqli_connect_errno()) {
    watchdog('FUDforum', 'Unable to connect to database: '. mysqli_connect_error());
    return FALSE;
  }

  return $fuddb;
}

function _fudforum_query($fuddb, $qstr) {
  $qstr = preg_replace('/{([^}]+)}/', $GLOBALS['FUD_PREFIX'] . '\\1', $qstr);
  $q = mysqli_query($fuddb, $qstr);
  return $q;
}

function _fudforum_disconnect($fuddb) {
  mysqli_close($fuddb);
}

/**
 * Implement hook_help() to display a small help message
 * if somebody clicks the "Help" link on the modules list.
 */
function fudforum_help($path, $arg) {
  switch ($path) {
    case 'admin/help#fudforum':
      return t('<h3>This module provides integration with FUDforum</h3><p>Users will be able to log in to Drupal with their FUDforum userid and passwords.</p><p>Blocks that can be enabled:<ul><li>New forum posts<li>Online forum users<li>Forum statistics</ul></p><p>Roles that can be assigned special permissions:<ul><li>forum user<li>forum moderator</ul></p>');
      break;
  }
}

/**
 * Implements hook_menu().
 * Define menu items and page callbacks.
 */
function fudforum_menu() {
  $items = array();
  $items['admin/settings/fudforum'] = array(
    'title' => 'FUDforum settings',
    'description' => 'Change FUDforum integration settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('fudforum_admin_settings'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Admin settings form
 */
function fudforum_admin_settings() {
  // Only administrators can access this page
  if (!user_access('access administration pages'))
    return message_access();

  // Create roles if the don't exist.
  $role = new stdClass();
  if (!user_role_load_by_name('forum user')) {
    $role->name = 'forum user';
    user_role_save($role);
  }
  if (!user_role_load_by_name('forum moderator')) {
    $role->name = 'forum user';
    user_role_save($role);
  }

  $form = array();
  $form['fudstatus'] = array('#type' => 'fieldset', 
    '#title' => t('FUDforum status'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE);

  $fudinc = variable_get('fudforum_inc', '/path/to/GLOBALS.php');
  if (file_exists($fudinc)) {
    _fudforum_settings();
    if (strpos($GLOBALS['DBHOST_DBTYPE'], 'mysql') === FALSE) {
      $form['fudstatus']['info0'] = array('#type' => 'markup', 
        '#markup' => '<div class="messages error">Integration is only supported if both DRUPAL and FUDforum uses MySQL. Other databases are not currently supported!</div>');
    }
    else {
      $fuddb = _fudforum_connect();
      if (!$fuddb) 
        $form['fudstatus']['info3'] = array('#type' => 'markup', 
          '#markup' => '<div class="messages error">Unable to connect to the FUDforum database. Please fix your forum settings!</div>');
      else {
        $form['fudstatus']['info4'] = array('#type' => 'markup', 
          '#markup' => '<div class="message ok">Successfully connected to the FUDforum database. Well done!</div>');
        _fudforum_disconnect($fuddb);
      }
    }
  }
  else {
    $form['fudstatus']['info5'] = array('#type' => 'markup', 
      '#markup' => '<div class="messages error">Error locating the GLOBALS.php file. Please specify the correct path to FUDforum\'s GLOBALS.php file and ensure the file is readable.</div>');
  }

  $form['fudsettings'] = array('#type' => 'fieldset', 
    '#title' => t('FUDforum settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE);

  $form['fudsettings']['fudforum_inc'] = array('#type' => 'textfield', 
    '#title' => t('Path to GLOBALS.php'), 
    '#maxlength' => '128', '#size' => '60',
    '#default_value' => $fudinc, 
    '#description' => t('Enter the full directory path where FUDforum\'s GLOBALS.php file is located.'));
  $form['fudsettings']['fudforum_root'] = array('#type' => 'textfield', 
    '#title' => t('FUDforum root'), 
    '#maxlength' => '30', '#size' => '30',
    '#default_value' => variable_get('fudforum_root', 'index.php'),
    '#description' => t('File to call in forum directory. Leave empty if your\'ve removed index.php from the forum URL.'));

  return system_settings_form($form);
}

/**
 * Implements hook_init().
 * Perform setup tasks for non-cached page requests.
 */
function fudforum_init() {
  global $user;

  // Try to log user in
  if (!$user->uid) {
     _fudforum_settings(); // Get FUDforum's cookie name
     if ( !empty($_COOKIE[ $GLOBALS['COOKIE_NAME'] ]) ) {
       $sess = $_COOKIE[ $GLOBALS['COOKIE_NAME'] ];
       if (!empty($sess)) {
         // drupal_set_message("Forum cookie found, try autologin");
         $fuddb = _fudforum_connect();
         $q = _fudforum_query($fuddb, 'SELECT u.login, u.passwd FROM {ses} s INNER JOIN {users} u ON u.id=(CASE WHEN u.id > 1 AND s.user_id>2000000000 THEN 1 ELSE s.user_id END) WHERE s.ses_id = "' . mysqli_real_escape_string($sess) . '"');
         if ($r = mysqli_fetch_row($q)) {
           if ($uid = fudforum_authenticate($r[0], $r[1])) {
              $user = user_load($uid, TRUE);
              drupal_session_regenerate();
              drupal_set_message(t('Successfully authenticated from forum DB.'));
           }
         }
         _fudforum_disconnect($fuddb);
       }
    }
  }
}

/**
 * Implements hook_block_info().
 * Define all blocks provided by the module.
 */
function fudforum_block_info() {
  $blocks = array();

  $blocks[1] = array(
    'info'  => t('FUDforum: New forum posts'),
  );
  $blocks[2] = array(
    'info'  => t('FUDforum: Online forum users'),
  );
  $blocks[3] = array(
    'info'  => t('FUDforum: Forum statistics'),
  );

  return $blocks;
}

function fudforum_block_view($delta = 0) {
  global $user;
  $block = array();

  switch ($delta) {
    case 1:
      $fuddb = _fudforum_connect();
      if ($fuddb) { 
        $q = _fudforum_query($fuddb, 'SELECT distinct thread_id, subject FROM {msg} WHERE apr=1 ORDER BY post_stamp DESC LIMIT 10');
        $c = '<div class="item-list"><ul>';
        while ( $r = mysqli_fetch_row($q) ) {
          $c .= '<li><a href="' . $GLOBALS['FUD_URL'] . 't/' . $r[0] . '/0/">' . $r[1] . '</a><br/>';
        }
        _fudforum_disconnect($fuddb);
        $c .= '</ul></div>';
        $c .= '<div class="more-link"><a href="' . $GLOBALS['FUD_URL'] . '" title="Enter the Forum.">more</a></div>';
      }

      $block['subject'] = t('New forum posts');
      $block['content'] = $c;
      return $block;

    case 2:
      $fuddb = _fudforum_connect();
      if ($fuddb) { 
        $tm_expire = REQUEST_TIME - ($GLOBALS['FUD_TIMEOUT'] * 60);
        $q = _fudforum_query($fuddb, 'SELECT u.id, u.alias, s.time_sec, (u.users_opt &32768) as private FROM {ses} s INNER JOIN {users} u ON s.user_id=u.id WHERE time_sec>' . $tm_expire);
        $c = NULL;
        while ( $r = mysqli_fetch_row($q) ) {
          $c .= '<li><a href="' . $GLOBALS['FUD_URL'] . 'u/' . $r[0] . '/0/">' . $r[1] . '</a><br/>';
        }
        _fudforum_disconnect($fuddb);
        if (isset($c))
          $c = '<div class="item-list"><ul>' . $c . '</ul></div>';
        else
          $c = 'Nobody';
      }
      $block['subject'] = t('Online forum users');
      $block['content'] = $c;
      return $block;

    case 3:
      $fuddb = _fudforum_connect();
      if ($fuddb) { 
        $q = _fudforum_query('$fuddb, SELECT count(*) FROM {users}');
        $r = mysqli_fetch_row($q);
        $c = 'Registered users: ' . $r[0] . '<br />';
        $q = _fudforum_query($fuddb, 'SELECT count(*) FROM {msg}');
        $r = mysqli_fetch_row($q);
        $c .= 'Messages posted: ' . $r[0] . '<br />';
        $q = _fudforum_query($fuddb, 'SELECT count(*) FROM {thread}');
        $r = mysqli_fetch_row($q);
        $c .= 'Threads: ' . $r[0] . '<br />';
        $tm_expire = REQUEST_TIME - ($GLOBALS['FUD_TIMEOUT'] * 60);
        $q = _fudforum_query($fuddb, 'SELECT count(*) FROM {ses} WHERE time_sec>' . $tm_expire . ' AND user_id<2000000000');
        $r = mysqli_fetch_row($q);
        $c .= 'Users online: ' . $r[0] . '<br />';
        $q = _fudforum_query($fuddb, 'SELECT MAX(id) FROM {users}');
        $r = mysqli_fetch_row($q);
        $q = _fudforum_query($fuddb, 'SELECT id, alias FROM {users} WHERE id=' . $r[0]);
        $r = mysqli_fetch_row($q);
        $c .= 'Newest user: <a href="' . $GLOBALS['FUD_URL'] . 'u/' . $r[0] . '/0/">' . $r[1] . '</a><br />';
      }

      $block['subject'] = t('Forum statistics');
      $block['content'] = $c;
      return $block;
  }
}

/**
 * Implementation of hook_form_alter().
 * We replace the local login validation handler with our own.
 */
function fudforum_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'user_login' || $form_id == 'user_login_block') {
    if (isset($form_state['input']['name'])) {
      $final = array_pop($form['#validate']);
      $form['#validate'][] = 'fudforum_login_validate';
      $form['#validate'][] = $final;
    }
  }
}

/**
 * Login form validation handler.
 */
function fudforum_login_validate($form, &$form_state) {
  global $user;

  if (!empty($user->uid)) {
    // Another module already handled authentication
    return;
  }
  // Call our custom authentication function
  $login  = $form_state['values']['name'];
  $passwd = $form_state['values']['pass'];
  if ($uid = fudforum_authenticate($login, $passwd)) {
    $form_state['uid'] = $uid;
    drupal_set_message(t('Successfully authenticated from forum DB.'));
  }
  else {
    // Not a forum user, try to use the standard Drupal authentication function
    user_login_authenticate_validate( $form, $form_state );
  }
}

/**
 * Custom authentication function
 */
function fudforum_authenticate($login, $passwd) {
  $uid = FALSE;

  $fuddb = _fudforum_connect();
  if ($fuddb && !empty($login) && !empty($passwd) ) {
    $q = _fudforum_query($fuddb, 'SELECT email, join_date, time_zone, sig, ban_expiry, name, location, occupation, users_opt, passwd, salt FROM {users} WHERE login=\'' . $login . '\'');
    $r = mysqli_fetch_row($q);
    _fudforum_disconnect($fuddb);
  }

  if (empty($r))
    return FALSE;  // user not found

  // r[9] == passwd, r[10] == salt
  if ($r[9] != $passwd && !((empty($r[10]) && $r[9] == md5($passwd)) || $r[9] == sha1($r[10] . sha1($passwd))) ) {
    return FALSE;  // wrong password
  }

  // User status: 1=active, 0=banned
  $status = ($r[4] == 0) ? 1 : 0;
  if ($status != 1) 
    return FALSE;  // user is banned

  // Get role
  $roles = user_roles(1);
  if ($r[8] &524288 || $r[8] &1048576 ) // 524288=is_mod, 1048576=is_admin
     $rname = 'forum moderator';
  else
     $rname = 'forum user';
  $rid = array_search($rname, $roles);

  // Load user to see if it was previously registered
  $account = user_load_by_name($login);

  if (empty($account->uid)) { // Register new user
    $user_default = array(
        'name'               => $login,
        // 'pass' => '', !!!NEVER SET PASSWORD, NOT EVEN TO ''!!!
        'pass'               => $passwd,
        'mail'               => $r[0],
        'created'            => $r[1],
        'timezone'           => $r[2],
        'signature'          => $r[3],
        'init'               => db_escape_table($login),
        'status'             => $status,
        'authname_module'    => 'fudforum',
        'roles'              => array($rid => $rname),
//        'profile_name'       => $r[5],
//        'profile_location'   => $r[6],
//        'profile_occupation' => $r[7]
    );

    // Now save the user.
    $account = user_save('', $user_default);
    watchdog('FUDforum', 'Register forum user: ' . $login, array(), WATCHDOG_NOTICE, l(t('edit'), 'user/' . $account->uid . '/edit'));
  }
  else {
    // Update changed user settings
    $account = user_save($account, array('pass' => $passwd));
    if ( $account->mail != $r[0] )
      $account = user_save($account, array('mail' => $r[0]));
    if ( $account->status != $status )
      $account = user_save($account, array('status' => $status));
    if ( $account->timezone != $r[2] )
      $account = user_save($account, array('timezone' => $r[2]));
//    if ( $account->profile_name != $r[5] )
//      $account = user_save($account, array('profile_name' => $r[5]));
//    if ( $account->profile_location != $r[6] )
//      $account = user_save($account, array('profile_location' => $r[6]));
//    if ( $account->profile_occupation != $r[7] )
//      $account = user_save($account, array('profile_occupation' => $r[7]));
    if ( ! isset($account->roles[$rid]) )
      $account = user_save($account, array('roles' => array($rid => $rname)));
  }

  return $account->uid;
}

/**
 * Implement hook_user_logout to destroy FUDforum cookie
 */
function fudforum_user_logout() {
  global $user;
  if ( in_array('forum user', $user->roles) || in_array('forum moderator', $user->roles) ) {
     _fudforum_settings();
     setcookie($GLOBALS['COOKIE_NAME'], FALSE, REQUEST_TIME - 86400, '/', $GLOBALS['COOKIE_DOMAIN']);
  }
}

/**
 * Implement hook_user_profile_form_alter() to disable the ability to change email address and
 * password for externally authenticated users.
 */
function fudforum_form_user_profile_form_alter( &$form, $form_state ) {
  global $user;
  if ( in_array('forum user', $user->roles) || in_array('forum moderator', $user->roles) ) {
     $form['account']['name']['#disabled'] = TRUE;
     $form['account']['name']['#description'] = t('Go to the forum to change your username.');
     $form['account']['mail']['#disabled'] = TRUE;
     $form['account']['mail']['#description'] = t('The e-mail address for this account can only be changed from the forum.');
     $form['account']['current_pass']['#disabled'] = TRUE;
     $form['account']['current_pass']['#description'] = t('The email address or password for this account can only be changed from the forum.');
     $form['account']['pass']['#disabled'] = TRUE;
     $form['account']['pass']['#description'] = t('The password for this account can only be changed from the forum.');
  }
}
